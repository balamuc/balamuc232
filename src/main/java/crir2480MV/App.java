package crir2480MV;

import crir2480MV.controller.MainGUIController;
import crir2480MV.gui.KitchenGUI;
import crir2480MV.model.PaymentType;
import crir2480MV.repository.MenuRepository;
import crir2480MV.repository.PaymentRepository;
import crir2480MV.service.PaymentValidator;
import crir2480MV.service.PizzaService;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.Optional;

/**
 * Hello world!
 */
public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        MenuRepository repoMenu = new MenuRepository("data/menu.txt");
        PaymentRepository payRepo = new PaymentRepository("data/payments.txt", new PaymentValidator());
        PizzaService service = new PizzaService(repoMenu, payRepo);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/mainFXML.fxml"));
        Parent box = loader.load();
        MainGUIController ctrl = loader.getController();
        ctrl.setService(service);
        primaryStage.setTitle("PizeriaX");
        primaryStage.setResizable(false);
        primaryStage.setAlwaysOnTop(false);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION, "Would you like to exit the Main window?", ButtonType.YES, ButtonType.NO);
                Optional<ButtonType> result = exitAlert.showAndWait();
                if (result.get() == ButtonType.YES) {
                    final String cashPaymentAlertMessage = "Incasari cash: " + service.getTotalAmount(PaymentType.CASH);
                    final String cardPaymentAlertMessage = "Incasari card: " + service.getTotalAmount(PaymentType.CARD);
                    System.out.println(cashPaymentAlertMessage);
                    System.out.println(cardPaymentAlertMessage);
                    Alert cashAlert = new Alert(Alert.AlertType.INFORMATION);
                    cashAlert.setTitle("Incasari final de zi");
                    cashAlert.setContentText(cashPaymentAlertMessage + "\n" + cardPaymentAlertMessage);
                    cashAlert.show();
                    primaryStage.close();
                }
                // consume event
                else if (result.get() == ButtonType.NO) {
                    event.consume();
                } else {
                    event.consume();

                }

            }
        });
        primaryStage.setScene(new Scene(box));
        primaryStage.show();
        KitchenGUI kitchenGUI = new KitchenGUI();
        kitchenGUI.KitchenGUI();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
