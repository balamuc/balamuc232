package crir2480MV.repository;


import crir2480MV.model.Payment;
import crir2480MV.model.PaymentType;
import crir2480MV.service.PaymentValidator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class PaymentRepository {
    private static String filename="data/payments.txt";
    private List<Payment> paymentList;
    private PaymentValidator validator;

//    public PaymentRepository(String filename) {
//        this.paymentList = new ArrayList<>();
//        this.filename = filename;
//        readPayments();
//    }

    public PaymentRepository(String filename, PaymentValidator validator) {
        this.paymentList = new ArrayList<>();
        this.filename = filename;
        this.validator = validator;
        readPayments();
    }

    public PaymentRepository(PaymentValidator validator) {
        this.validator = validator;
        this.paymentList = new ArrayList<>();
    }

    public void deleteAll() {
        this.paymentList.clear();
    }

    private void readPayments() {
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = br.readLine()) != null) {
                Payment payment = getPayment(line);
                paymentList.add(payment);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Payment getPayment(String line) {
        Payment item = null;
        if (line == null || line.equals("")) return null;
        StringTokenizer st = new StringTokenizer(line, ",");
        int tableNumber = Integer.parseInt(st.nextToken());
        String type = st.nextToken();
        double amount = Double.parseDouble(st.nextToken());
        item = new Payment(tableNumber, PaymentType.valueOf(type), amount);
        return item;
    }

    public void add(Payment payment) {
        if (validator.validate(payment)) {
            paymentList.add(payment);
            writeAll();
        }

    }
    public void addInMemory(Payment payment){
        if (validator.validate(payment)) {
            paymentList.add(payment);
        }
    }

    public List<Payment> getAll() {
        return paymentList;
    }

    public void writeAll() {
        ClassLoader classLoader = PaymentRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file));
            for (Payment p : paymentList) {
                System.out.println(p.toString());
                bw.write(p.toString());
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
