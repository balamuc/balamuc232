package crir2480MV.service;

import crir2480MV.model.PaymentType;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class PaymentAlert implements PaymentOperation {
    private PizzaService service;
    private static final String OUTPUT_LINE="--------------------------";
    public PaymentAlert(PizzaService service){
        this.service=service;
    }

    @Override
    public void cardPayment() {

        System.out.println(OUTPUT_LINE);
        System.out.println("Paying by card...");
        System.out.println("Please insert your card!");
        System.out.println(OUTPUT_LINE);
    }
    @Override
    public void cashPayment() {
        System.out.println(OUTPUT_LINE);
        System.out.println("Paying cash...");
        System.out.println("Please show the cash...!");
        System.out.println(OUTPUT_LINE);
    }
    @Override
    public void cancelPayment() {
        System.out.println(OUTPUT_LINE);
        System.out.println("Payment choice needed...");
        System.out.println(OUTPUT_LINE);
    }
      public void showPaymentAlert(int tableNumber, double totalAmount ) {
        Alert paymentAlert = new Alert(Alert.AlertType.CONFIRMATION);
        paymentAlert.setTitle("Payment for Table "+tableNumber);
        paymentAlert.setHeaderText("Total amount: " + totalAmount);
        paymentAlert.setContentText("Please choose payment option");
        ButtonType cardPayment = new ButtonType("Pay by Card");
        ButtonType cashPayment = new ButtonType("Pay Cash");
        ButtonType cancel = new ButtonType("Cancel");
        paymentAlert.getButtonTypes().setAll(cardPayment, cashPayment, cancel);
        Optional<ButtonType> result = paymentAlert.showAndWait();
        if (result.isPresent() && result.get() == cardPayment) {
            cardPayment();
            service.addPayment(tableNumber, PaymentType.CARD,totalAmount);
        } else if (result.isPresent() && result.get() == cashPayment) {
            cashPayment();
            service.addPayment(tableNumber, PaymentType.CASH,totalAmount);
        } else if (result.isPresent() && result.get() == cancel) {
             cancelPayment();
        } else {
            cancelPayment();
        }
    }
}
