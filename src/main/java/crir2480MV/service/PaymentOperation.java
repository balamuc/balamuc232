package crir2480MV.service;

public interface PaymentOperation {
     void cardPayment();
     void cashPayment();
     void cancelPayment();
}
