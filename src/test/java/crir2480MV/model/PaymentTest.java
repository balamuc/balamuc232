package crir2480MV.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {
    Payment payment = new Payment(5,PaymentType.CARD,76.4);

    @Test
    void getTableNumber() {
        assertEquals(5,payment.getTableNumber());
    }

    @Test
    void getType() {
        assertEquals(PaymentType.CARD,payment.getType());
    }

    @Test
    void getAmount() {
        assertEquals(76.4,payment.getAmount());
    }
}