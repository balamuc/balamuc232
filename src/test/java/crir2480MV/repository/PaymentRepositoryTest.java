package crir2480MV.repository;

import crir2480MV.model.Payment;
import crir2480MV.model.PaymentType;
import crir2480MV.service.PaymentValidator;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class PaymentRepositoryTest {
    @Mock
    private PaymentValidator validator;
    @InjectMocks
    private PaymentRepository repository = new PaymentRepository(validator);
    @Mock
    private Payment payment;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addInMemory() {
        Mockito.when(validator.validate(payment)).thenReturn(Boolean.TRUE);
        repository.addInMemory(payment);
        assertEquals(1, repository.getAll().size());
        Mockito.verify(validator, Mockito.times(1)).validate(payment);
    }

    @Test
    void getAll() {
        Mockito.when(validator.validate(payment)).thenReturn(Boolean.TRUE);
        repository.addInMemory(payment);
        repository.addInMemory(payment);
        repository.addInMemory(payment);
        assertEquals(3, repository.getAll().size());
        Mockito.verify(validator, Mockito.times(3)).validate(payment);
    }


}