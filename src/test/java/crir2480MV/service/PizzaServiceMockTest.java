package crir2480MV.service;

import crir2480MV.model.Payment;
import crir2480MV.model.PaymentType;
import crir2480MV.repository.PaymentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PizzaServiceMockTest {

    @Mock
    private PaymentRepository repository;
    @InjectMocks
    private PizzaService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addPayment() {
        Payment payment1 = new Payment(7, PaymentType.CASH, 20);
        Mockito.when(repository.getAll()).thenReturn(Arrays.asList(payment1));
        Mockito.doNothing().when(repository).add(payment1);
        service.addPaymentObject(payment1);
        Mockito.verify(repository,Mockito.times(1)).addInMemory(payment1);
        assertEquals(1,service.getPayments().size());
        Mockito.verify(repository,Mockito.times(1)).getAll();
    }


    @Test
    void getTotalAmount() {
        Payment payment1 = new Payment(8, PaymentType.CASH, 30.4);
        Payment payment2 = new Payment(5, PaymentType.CARD, 76.4);
        Mockito.when(repository.getAll()).thenReturn(Arrays.asList(payment1, payment2));
        double value = service.getTotalAmount(PaymentType.CASH);
        Mockito.verify(repository,Mockito.times(1)).getAll();
        assertEquals(30.4,value);
    }
}