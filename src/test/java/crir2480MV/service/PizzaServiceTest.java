package crir2480MV.service;

import crir2480MV.model.Payment;
import crir2480MV.model.PaymentType;
import crir2480MV.repository.MenuRepository;
import crir2480MV.repository.PaymentRepository;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Special test case")
class PizzaServiceTest {

    private static PizzaService pizzaService;
    private static MenuRepository menuRepository;
    private static PaymentRepository paymentRepository;

    @BeforeAll
    static void setup() {
        menuRepository = new MenuRepository("test_data/menu.txt");
        paymentRepository = new PaymentRepository("test_data/payments.txt", new PaymentValidator());
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @BeforeEach
    void clear() {
        paymentRepository.deleteAll();
    }

    @Test
    void TC1_ECP() {
        pizzaService.addPayment(3, PaymentType.CASH, 566.13);
        List<Payment> paymentList = paymentRepository.getAll();
        assertEquals(paymentList.size(), 1);
    }

    @Test
    void TC2_ECP() {
        pizzaService.addPayment(3, PaymentType.CASH, -10.56);
        List<Payment> paymentList = paymentRepository.getAll();
        assertEquals(paymentList.size(), 0);
    }

    @Test
    void TC3_ECP() {
        pizzaService.addPayment(9, PaymentType.CASH, 48.12);
        List<Payment> paymentList = paymentRepository.getAll();
        pizzaService.getMenuData();
        assertEquals(paymentList.size(), 0);
    }

    @Test
    void TC4_BVA() {
        pizzaService.addPayment(1, PaymentType.CASH, 1.00);
        List<Payment> paymentList = paymentRepository.getAll();
        assertEquals(paymentList.size(), 1);
    }

    @Test
    void TC5_BVA() {
        pizzaService.addPayment(0, PaymentType.CASH, 1.01);
        List<Payment> paymentList = paymentRepository.getAll();
        assertEquals(paymentList.size(), 0);
    }

    @Test
    void TC6_BVA() {
        pizzaService.addPayment(2, PaymentType.CASH, 0.99);
        List<Payment> paymentList = paymentRepository.getAll();
        assertEquals(paymentList.size(), 0);
    }

    @Test
    void TC7_BVA() {
        pizzaService.addPayment(8, PaymentType.CASH, 9999);
        List<Payment> paymentList = paymentRepository.getAll();
        assertEquals(paymentList.size(), 1);
    }

    @Test
    void TC8_BVA() {
        pizzaService.addPayment(7, PaymentType.CASH, 10000);
        List<Payment> paymentList = paymentRepository.getAll();
        assertEquals(paymentList.size(), 1);
    }
    @RepeatedTest(3)
    @Tag("Repeated test")
    @Test
    void TC9_BVA() {
        pizzaService.addPayment(9, PaymentType.CASH, 1);
        List<Payment> paymentList = paymentRepository.getAll();
        assertEquals(paymentList.size(), 0);
    }

    @Test
    void TC10_BVA() {
        pizzaService.addPayment(1, PaymentType.CASH, 10001);
        List<Payment> paymentList = paymentRepository.getAll();
        assertEquals(paymentList.size(), 0);
    }
    @Test
    @Disabled
    void disabled_test() {
        pizzaService.addPayment(1, PaymentType.CASH, 3);
        List<Payment> paymentList = paymentRepository.getAll();
        assertEquals(paymentList.size(), 0);
    }

    @Test
    void TC11_WBT(){
        double totalAmount = pizzaService.getTotalAmount(PaymentType.CASH);
        assertEquals(0, totalAmount);
    }

    @Test
    void TC12_WBT(){
        pizzaService.addUnvalidatedPayment(-1, PaymentType.CASH, 1.00);
        double totalAmount = pizzaService.getTotalAmount(PaymentType.CASH);
        assertEquals(0, totalAmount);
    }

    @Test
    void TC13_WBT(){
        pizzaService.addUnvalidatedPayment(6, PaymentType.CASH, 25.00);
        double totalAmount = pizzaService.getTotalAmount(PaymentType.CASH);
        assertEquals(25.00, totalAmount);
    }

    @Test
    void TC14_WBT(){
        pizzaService.addUnvalidatedPayment(1, PaymentType.CASH, -25.00);
        double totalAmount = pizzaService.getTotalAmount(PaymentType.CASH);
        assertEquals(0, totalAmount);
    }





}