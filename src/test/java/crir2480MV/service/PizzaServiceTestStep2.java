package crir2480MV.service;

import crir2480MV.model.Payment;
import crir2480MV.repository.MenuRepository;
import crir2480MV.repository.PaymentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class PizzaServiceTestStep2 {

    private PaymentRepository paymentRepository;
    private PizzaService service;
    private MenuRepository menuRepository;

    @Mock
    private PaymentValidator paymentValidator;

    @Mock
    private Payment payment;
    @Mock
    private Payment payment2;
    @Mock
    private Payment payment3;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        menuRepository = new MenuRepository("test_data/menu.txt");
        paymentRepository = new PaymentRepository(paymentValidator);
        service = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    void getPayments() {
        Mockito.when(paymentValidator.validate(any())).thenReturn(true);
        service.addPaymentObject(payment);
        service.addPaymentObject(payment2);
        service.addPaymentObject(payment3);
        assertEquals(3, service.getPayments().size());
        Mockito.verify(paymentValidator, Mockito.times(3)).validate(any());
    }

    @Test
    void addPayment() {
        Mockito.when(paymentValidator.validate(payment)).thenReturn(true);
        service.addPaymentObject(payment);
        service.addPaymentObject(payment);
        assertEquals(2, service.getPayments().size());
    }
}