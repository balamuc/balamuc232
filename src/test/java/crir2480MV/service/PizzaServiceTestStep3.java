package crir2480MV.service;

import crir2480MV.model.Payment;
import crir2480MV.model.PaymentType;
import crir2480MV.repository.MenuRepository;
import crir2480MV.repository.PaymentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class PizzaServiceTestStep3 {

    private PaymentRepository paymentRepository;
    private PizzaService service;
    private MenuRepository menuRepository;
    private PaymentValidator paymentValidator;
    @BeforeEach
    void setUp() {
        paymentValidator = new PaymentValidator();
        menuRepository = new MenuRepository("test_data/menu.txt");
        paymentRepository = new PaymentRepository(paymentValidator);
        service = new PizzaService(menuRepository, paymentRepository);
    }
    @Test
    void addPaymentObject() {
        Payment payment = new Payment(7, PaymentType.CASH,86.31);
        assertEquals(0,service.getPayments().size());
        service.addPaymentObject(payment);
        assertEquals(1,service.getPayments().size());
    }

    @Test
    void getTotalAmount() {
        Payment payment = new Payment(7, PaymentType.CASH,86.31);
        Payment payment1 = new Payment(7, PaymentType.CARD,20);
        Payment payment2 = new Payment(7, PaymentType.CASH,63);
        Payment payment3 = new Payment(7, PaymentType.CARD,86.14);
        service.addPaymentObject(payment);
        service.addPaymentObject(payment1);
        service.addPaymentObject(payment2);
        service.addPaymentObject(payment3);
        double cardValue = service.getTotalAmount(PaymentType.CARD);
        double cashValue = service.getTotalAmount(PaymentType.CASH);
        assertEquals(106.14,cardValue);
        assertEquals(149.31,cashValue);
    }
}